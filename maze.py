# https://en.wikipedia.org/wiki/Maze_generation_algorithm#Python_code_example
# Modified Prim's

import numpy
from numpy.random import randint
import sys

class Maze:
    def __init__(self, width, height, complexity, density, seed=None):
        # handle seed=None
        if seed:
            inseed = seed
        else:
            inseed = randint(0, 2**32 - 1)
        # seed the generator
        numpy.random.seed(inseed)
        self.seed = inseed

        # Only odd shapes
        shape = ((height // 2) * 2 + 1, (width // 2) * 2 + 1)
        # Adjust complexity and density relative to maze size
        # number of components
        complexity = int(complexity * (5 * (shape[0] + shape[1])))
        # size of components
        density    = int(density * ((shape[0] // 2) * (shape[1] // 2)))
        # Build actual maze
        Z = numpy.zeros(shape, dtype=bool)
        # Fill borders
        Z[0, :] = Z[-1, :] = 1
        Z[:, 0] = Z[:, -1] = 1
        # Make aisles
        for i in range(density):
            # pick a random position
            x, y = randint(0, shape[1] // 2) * 2, randint(0, shape[0] // 2) * 2
            Z[y, x] = 1
            for j in range(complexity):
                neighbours = []
                if x > 1:             neighbours.append((y, x - 2))
                if x < shape[1] - 2:  neighbours.append((y, x + 2))
                if y > 1:             neighbours.append((y - 2, x))
                if y < shape[0] - 2:  neighbours.append((y + 2, x))
                if len(neighbours):
                    y_,x_ = neighbours[randint(0, len(neighbours) - 1)]
                    if Z[y_, x_] == 0:
                        Z[y_, x_] = 1
                        Z[y_ + (y - y_) // 2, x_ + (x - x_) // 2] = 1
                        x, y = x_, y_

        # make an exit
        # TODO: make sure there isn't a perpendicular wall at this point
        side = randint(0,3)
        (y,x) = {
            0: lambda: (0,                           randint(0, shape[1]-1)),
            1: lambda: (randint(1, shape[0]-1),  0),
            2: lambda: (shape[0]-1,                  randint(0, shape[1]-1)),
            3: lambda: (randint(0, shape[0]),        shape[1]-1)
        }[side]()
        Z[y,x] = 0
        
        self.exit = (x, y)
        self.maze = Z
