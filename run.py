import argparse
import matplotlib.pyplot as pyplot

from maze import Maze

parser = argparse.ArgumentParser(description='generate a random maze')
parser.add_argument('--seed', metavar='s', dest= 'seed', type=int,
                    help='integer seed for maze generation')
parser.add_argument('--width', metavar='w', dest='width', type=int,
                    default=51, help='maze width')
parser.add_argument('--height', metavar='y', dest='height', type=int,
                    default=35, help='maze height')
parser.add_argument('--complexity', metavar='c', dest='complexity', type=float,
                    default=0.75, help='maze complexity')
parser.add_argument('--density', metavar='d', dest='density', type=float,
                    default=0.75, help='density for the maze')
args = parser.parse_args()
args_dict = vars(args)

m = Maze(**args_dict)

print('random seed =',m.seed)
print('exit is at', m.exit)

pyplot.figure(figsize=(10, 5))
pyplot.imshow(m.maze, cmap=pyplot.cm.binary, interpolation='nearest')
pyplot.xticks([]), pyplot.yticks([])
pyplot.show()
