# Auto Bot Maze

Autonomous Robot Maze

Teaching platform to introduce students to programming in python.  Fill out callback stub to navigate a virtual robot through a randomly-generated maze.  Originally created for [HopeIT](https://hopeit.net), hopefully others will find it useful as well.

Highlights/TODO:

* build maze from random seed, pick exit and starting point based on that seed
* output seed, and accept it as input
  * this way a student can retry an exact maze they failed before after tweaking bot code
* variable scale on the generated mazes
  * so that we can generate small/easy mazes as well as large/complex
* maybe: flag to generate maze not solvable by right hand rule
  * this might be harder given the random generation
* structured as a library that can be invoked with code built to its interface
* I think the exit shouldn’t need to be on the edge, so pick an x,y (that isn’t a wall) and make that an exit
* same with starting point, pick an x,y.
  * thought:  make the starting point and the exit be in separate quadrants of the maze
* runner class
  * composes the Maze object
  * and validates moves from the callback it is given
  * student can’t get to the maze object directly to ask it for the exit coordinates (or, if they can, maybe they’re in the wrong class)
  * need to come up with a first stab on the interface that the callback receives…
    * probably a Location object
        * exposes x, y, and walls to n, s, e, w.  (any other data)
* display: matlabplot won’t work long term
  * want something prettier, with a sprite for the robot and the exit
* sizing beyond explicit x,y.  something like t-shirt sizes, S, M, L, XL...

Dependencies:

* matplotlib
* numpy
